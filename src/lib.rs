#![doc = include_str!("../readme.md")]

mod actor;
pub mod error;
/// Provides tools for parsing gemtext
pub mod parse;
mod response;
mod status;
mod titan;
mod user_agent;

pub use actor::{trot, trot_in, Actor};
pub use response::Response;
pub use status::Status;
/// Used for [`Actor::upload`]
pub use titan::Titan;
pub use user_agent::UserAgent;
