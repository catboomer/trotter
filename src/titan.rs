pub struct Titan {
	pub content: Vec<u8>,
	pub mimetype: String,
	pub token: Option<String>,
}
